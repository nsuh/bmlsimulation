\name{createBMLGrid}
\alias{createBMLGrid}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Creates the grid, called cars.
}
\description{
Creates the grid of cars.
This is a data frame with coordinates of map, and cars
}
\usage{
createBMLGrid(r = 100, c = 99, ncars = c(red = 100, blue = 100))
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{r}{
row size
}
  \item{c}{
column size
}
  \item{ncars}{
number of total cars, including both red and blue.  vector of length 2
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
returns grid, which is a data frame of type car
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Nathan Suh
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
g = createBMLGrid(r = 100, c = 99, ncars = c(red = 100, blue = 100))

## The function is currently defined as
function (r = 100, c = 99, ncars = c(red = 100, blue = 100)) 
{
    n = round(sum(ncars)/2)
    xLen = c
    yLen = r
    cars = generateCars(n, xLen, yLen)
    return(cars)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
